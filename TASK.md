# Fork this project and send back a pull request.

## What to do
Create a simple API by importing data from GitHub Search API https://developer.github.com/v3/search/

Import repositories that have been written in Python and have more than 500 stars. 

Import just a few fields from GitHub Search API: `full_name`, `html_url`, 
`description`, `stargazers_count`, `language`.

It is necessary to do a script to fill the database.
Add pagination to API.
 
## Bonus 1
Add sorting by `stars` to API.

## Bonus 2
Use docker compose encapsulating all the services related to this app.

## Some help
* How to fork https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html
* How to create a pull request https://confluence.atlassian.com/bitbucket/create-a-pull-request-774243413.html