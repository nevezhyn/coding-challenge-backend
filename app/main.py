"""
This module is used to start an application. It is intended only for
development purposes. For production environment more 'bullet proof'
runner is recomended (e.g. Gunicorn).
"""
import logging
from asyncio import get_event_loop, Lock

import yaml
from aiohttp import web, ClientSession

from gitstars.utils import network_config
from gitstars.handlers import repositories, repository, rebuild

if __name__ == '__main__':
    LOG_FILENAME = 'gitstars/static/logging.txt'
    logging.basicConfig(filename=LOG_FILENAME,
                        level=logging.DEBUG,
                        filemode='w')

    with open('gitstars/static/swagger.yaml') as f:
        swagger_spec = yaml.load(f)

    loop = get_event_loop()
    server_addr, db_addr = network_config()

    web_scheme, _, web_port, web_base = server_addr
    db_host, db_port = db_addr

    server = web.Application()
    server.router.add_route('*', web_base + '/repositories', repositories)
    server.router.add_route('*', web_base + '/repositories/{repository_id}',
                            repository)
    server.router.add_route('*', web_base + '/rebuild-repositories', rebuild)
    server['com.nevezhyn.gitstars.rebuild_lock'] = Lock()
    server['com.nevezhyn.gitstars.swagger_spec'] = swagger_spec
    server['com.nevezhyn.gitstars.client_session'] = ClientSession
    server.router.add_static("/static/", "gitstars/static/",
                             show_index=True,
                             append_version=True)
    web.run_app(server, loop=loop, host='0.0.0.0', port=web_port)
