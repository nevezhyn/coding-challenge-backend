"""
This module processes HTTP request and sends HTTP responses.
Only members of this module aware that they are part of aiohttp application.
"""
import ujson

from aiohttp import web

from gitstars import errors
from gitstars.utils import (
    log_exception,
    json_error_response,
    json_response,
    construct_repositories_rebuild
)
from gitstars.const import SUPPORTED_METHODS
from gitstars.processors import (
    get_repositories,
    post_repository,
    get_repository,
    delete_repository,
    rebuild_repos,
    put_repository
)
from gitstars.validators import (
    validate_request_headers,
    validate_repo_uuid,
    validate_and_parse_pagination,
    validate_and_parse_sorting,
    validate_json_object,
    validate_request_method
)


async def repositories(request):
    """
    Implements GET and POST logic of '/repositories' endpoint
    """
    headers = request.headers
    method = request.method
    supported_methods = SUPPORTED_METHODS['/repositories']

    # Validate the request method and headers
    try:
        validate_request_method(method, supported_methods)
        validate_request_headers(headers)
    except errors.WrongMethod as e:
        e.message_json = e.message_json.format(method, supported_methods)
        await log_exception(e)
        return json_error_response(e)
    except errors.AcceptTypeError as e:
        await log_exception(e)
        return json_error_response(e)
    except errors.ContentTypeError as e:
        await log_exception(e)
        return json_error_response(e)

    if request.method == 'GET':
        # Check for a rebuild lock
        rebuild_lock = request.app['com.nevezhyn.gitstars.rebuild_lock']
        if rebuild_lock.locked():
            return json_response(construct_repositories_rebuild(),
                                 status=202)

        query_parameters = request.query

        # Validate and parse pagination parameters
        try:
            page, per_page = await validate_and_parse_pagination(
                query_parameters
            )
        except errors.BadRequestQuery as e:
            e.message_json = 'Only int > 0 allowed for page and per_page.'
            await log_exception(e)
            return json_error_response(e)

        # Validate and parse sorting parameters
        try:
            sort_pairs = await validate_and_parse_sorting(query_parameters)
        except errors.BadRequestQuery as e:
            e.message_json = 'Wrong sorting keys parameters.'
            await log_exception(e)
            return json_error_response(e)

        # Get repositories
        try:
            result = await get_repositories(page, per_page, sort_pairs)
            return json_response(result)
        except errors.InternalServerError as e:
            e.message_json = 'Error while executing find_one in DB'
            return json_error_response(e)

    if request.method == 'POST':
        # Load JSON object
        try:
            repopository_to_create = await request.json(loads=ujson.loads)
        except Exception as e:
            await log_exception(e)
            return json_error_response(errors.BadRequestBody())

        # Validate JSON schema
        try:
            swagger_spec = request.app['com.nevezhyn.gitstars.swagger_spec']
            repo_obj_spec = swagger_spec['definitions']['repository']
            fields_to_digest = repo_obj_spec['required']
            await validate_json_object(repopository_to_create, repo_obj_spec)
        except errors.BadRequestBody as e:
            await log_exception(e)
            return json_error_response(e)
        except errors.GitHubNullDescription as e:
            log_exception(e)
            # Workaround when description: null (not a string) GitHubs issue...
            pass

        # Post repository
        try:
            new, result = await post_repository(repopository_to_create,
                                                fields_to_digest)
            status = 201 if new else 200
            return json_response(result, status=status)
        except errors.InternalServerError as e:
            await log_exception(e)
            return json_error_response(e)


async def repository(request):
    """
    Implements GET, POST and PUT logic of '/repositories/{repository_id}'
    endpoint
    """

    headers = request.headers
    method = request.method
    supported_methods = SUPPORTED_METHODS['/repository']

    # Validate the request method and headers
    try:
        validate_request_method(method, supported_methods)
        validate_request_headers(headers)
    except errors.WrongMethod as e:
        e.message_json = e.message_json.format(method, supported_methods)
        await log_exception(e)
        return json_error_response(e)
    except errors.AcceptTypeError as e:
        await log_exception(e)
        return json_error_response(e)
    except errors.ContentTypeError as e:
        await log_exception(e)
        return json_error_response(e)

    # Validate the UUID parameter
    try:
        repo_uuid = request.match_info['repository_id']
        validate_repo_uuid(repo_uuid)
    except errors.BadRequestQuery as e:
        e.message_json = 'Wrong repository ID'
        await log_exception(e)
        return json_error_response(e)

    # Check for a rebuild lock
    rebuild_lock = request.app['com.nevezhyn.gitstars.rebuild_lock']
    if rebuild_lock.locked():
        return json_response(construct_repositories_rebuild(),
                             status=202)

    if request.method == 'GET':
        try:
            present_repo = await get_repository(repo_uuid)
            return json_response(present_repo)
        except errors.ResourceNotFound as e:
            e.message_json = 'Repository {} not found'.format(repo_uuid)
            await log_exception(e)
            return json_error_response(e)
        except errors.InternalServerError as e:
            await log_exception(e)
            return json_error_response(e)

    if request.method == 'PUT':
        # Check that repository exists
        try:
            present_repo = await get_repository(repo_uuid)
        except errors.ResourceNotFound as e:
            e.message_json = 'Repository {} not found'.format(repo_uuid)
            await log_exception(e)
            return json_error_response(e)
        except errors.InternalServerError as e:
            await log_exception(e)
            return json_error_response(e)

        # Load JSON object
        try:
            new_repo = await request.json(loads=ujson.loads)
        except Exception as e:
            await log_exception(e)
            return json_error_response(errors.BadRequestBody())

        # Validate JSON schema
        try:
            swagger_spec = request.app['com.nevezhyn.gitstars.swagger_spec']
            repo_obj_spec = swagger_spec['definitions']['repository']
            keys_to_digest = repo_obj_spec['required']
            await validate_json_object(new_repo, repo_obj_spec)
        except errors.BadRequestBody as e:
            await log_exception(e)
            return json_error_response(e)

        # Post repository
        try:
            updated_repository = await put_repository(present_repo,
                                                      new_repo,
                                                      keys_to_digest)
            return json_response(updated_repository)
        except errors.InternalServerError as e:
            await log_exception(e)
            return json_error_response(e)

    if request.method == 'DELETE':
        try:
            await delete_repository(repo_uuid)
            return web.Response(status=204)
        except errors.ResourceNotFound as e:
            e.message_json = 'Repository {} not found'.format(repo_uuid)
            await log_exception(e)
            return json_error_response(e)
        except errors.InternalServerError as e:
            await log_exception(e)
            return json_error_response(e)


async def rebuild(request):
    """
    Implements POST logic of '/rebuild-repositories' endpoint
    """

    headers = request.headers
    method = request.method
    supported_methods = SUPPORTED_METHODS['/rebuild-repositories']

    # Validate the request method and headers
    try:
        validate_request_method(method, supported_methods)
        validate_request_headers(headers)
    except errors.WrongMethod as e:
        e.message_json = e.message_json.format(method, supported_methods)
        await log_exception(e)
        return json_error_response(e)
    except errors.AcceptTypeError as e:
        await log_exception(e)
        return json_error_response(e)
    except errors.ContentTypeError as e:
        await log_exception(e)
        return json_error_response(e)

    if request.method == 'POST':
        try:
            rebuild_lock = request.app['com.nevezhyn.gitstars.rebuild_lock']
            session = request.app['com.nevezhyn.gitstars.client_session']
            request.app.loop.create_task(
                rebuild_repos(rebuild_lock, session))
            return json_response(construct_repositories_rebuild(),
                                 status=202)
        except errors.ExternalResource as e:
            e.message_json = 'External resource error when rebuilding ' \
                             'collection.'
            await log_exception(e)
            return json_error_response(e)
        except errors.InternalServerError as e:
            e.message_json = 'Error while rebuilding collection.'
            await log_exception(e)
            return json_error_response(e)
