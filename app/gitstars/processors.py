"""
This module implements general workflow of requested operation.
All interfaces implemented on 'Pure Python Objects'.
This module does not handle errors. All handling should be implemented
by caller. All exceptions goes to outer scope.

"""
import ujson
from math import ceil
from uuid import uuid4

from gitstars.utils import (
    post_many,
    construct_collections_links,
    construct_items_links,
    get_digest,
    github_url,
    github_fetcher
)
from gitstars.const import COLLECTION_NAME
from gitstars.db_adaptor import (
    get_db_adaptor,
    pagenated_repositories,
    read_by_uuid,
    delete_by_uuid,
    repositories_count,
    clear_repositories,
    update_repository
)


async def get_repositories(page, per_page, sort_pairs):
    # Parse pagination and sorting parameters then read a collection
    # from the DB and return this collection
    total_items = await repositories_count()
    total_pages = ceil(total_items / per_page)
    links = construct_collections_links(page, per_page, total_pages)
    repositories = await pagenated_repositories(page=page,
                                                per_page=per_page,
                                                sort=sort_pairs)
    # Construct repositories links field
    repo_links = (construct_items_links(repo) for repo in repositories)
    for repo, link in zip(repositories, repo_links):
        repo['links'] = link

    collection = {
        'items': repositories,
        'total_items': total_items,
        'total_pages': total_pages,
        'links': links
    }

    return collection


async def put_repository(present_repo, new_repo, keys_to_digest):
    obj_to_digest = {key: value
                     for key, value in new_repo.items()
                     if key in keys_to_digest}
    new_digest = get_digest(ujson.dumps(obj_to_digest, sort_keys=True))

    # If repository was not changed then just return it
    if present_repo['digest'] == new_digest:
        return present_repo

    # Insert a record in DB then return this record
    present_repo_uuid = present_repo['uuid']
    repo_to_db = {key: value
                  for key, value in new_repo.items()
                  if key in keys_to_digest}
    repo_to_db['uuid'] = present_repo_uuid
    repo_to_db['digest'] = new_digest
    updated_repository = await update_repository(present_repo_uuid, repo_to_db)
    return updated_repository


async def post_repository(repopository_to_create, fields_to_digest):
    # If uuid in the DB then return this record
    db_adaptor = get_db_adaptor()
    repo_uuid = repopository_to_create.get('uuid')
    if repo_uuid:
        document_by_uuid = await db_adaptor.find_one(
            COLLECTION_NAME,
            filter={'repository_id': repo_uuid},
            projection={'_id': False}
        )
        if document_by_uuid:
            new = False
            return new, document_by_uuid

    # If same digest in the DB then return this record
    required_fields_digest = {key: value
                              for key, value in
                              repopository_to_create.items()
                              if key in fields_to_digest}
    digest = get_digest(ujson.dumps(required_fields_digest, sort_keys=True))
    search_filter = {'digest': digest}
    document_by_digest = await db_adaptor.find_one(COLLECTION_NAME,
                                                   filter=search_filter,
                                                   projection={
                                                       '_id': False})
    if document_by_digest:
        new = False
        return new, document_by_digest

    # Insert a record in DB then return this record
    required_fields_digest['uuid'] = 'REPO-' + uuid4().hex.upper()
    required_fields_digest['digest'] = digest
    result = await db_adaptor.insert_one(COLLECTION_NAME,
                                         required_fields_digest)
    created_id = result.inserted_id
    search_filter = {'_id': created_id}
    created_document = await db_adaptor.find_one(COLLECTION_NAME,
                                                 filter=search_filter,
                                                 projection={'_id': False})
    new = True
    return new, created_document


async def get_repository(repo_uuid):
    # Search for a record
    result = await read_by_uuid(repo_uuid)
    return result


async def delete_repository(repo_uuid):
    # Search for a record
    repository = await read_by_uuid(repo_uuid)
    await delete_by_uuid(repo_uuid)


async def rebuild_repos(rebuild_lock, session):
    recursive_aggregator = list()
    search_url = github_url()
    try:
        await rebuild_lock.acquire()
        await clear_repositories()
    finally:
        rebuild_lock.release()

    try:
        await rebuild_lock.acquire()
        async with session(json_serialize=ujson.dumps) as client_session:
            repos = await github_fetcher(client_session,
                                         search_url,
                                         recursive_aggregator)
            await post_many(repos, client_session)
    finally:
        rebuild_lock.release()
