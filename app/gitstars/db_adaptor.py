"""
This module is responsible for high and low level database operations.
"""
from asyncio import get_event_loop

import motor.motor_asyncio
from pymongo import ReturnDocument
from gitstars import errors
from gitstars.const import DB_NAME, COLLECTION_NAME
from gitstars.utils import log_exception, network_config

_, db_addr = network_config()
db_host, db_port = db_addr
loop = get_event_loop()
client = motor.motor_asyncio.AsyncIOMotorClient(db_host, db_port, io_loop=loop)
db = client[DB_NAME]


class DBAdapter:
    """
    This is a high level adapter over MongoDB.
    Low level methods mimics PyMongo function names.
    Database client and database objects are singletons (Borg actually).
    """

    def __init__(self):
        self._client = client
        self._db = db

    async def drop(self, collection_name):
        return await self._db[collection_name].drop()

    async def insert_many(self, collection_name, to_insert):
        return await self._db[collection_name].insert_many(to_insert)

    async def insert_one(self, collection_name, to_insert):
        return await self._db[collection_name].insert_one(to_insert)

    async def delete_one(self, collection_name, delete_filter):
        return await self._db[collection_name].delete_one(delete_filter)

    async def count(self, collection_name):
        return await self._db[collection_name].count()

    def find_cursor(self, collection_name, **kwargs):
        cursor = self._db[collection_name].find(**kwargs)
        return cursor

    async def find_one(self, collection_name, **kwargs):
        return await self._db[collection_name].find_one(**kwargs)

    async def find_one_and_update(self, colletion_name, filter,
                                  update, **kwargs):
        return await self._db[colletion_name].find_one_and_update(filter,
                                                                  update,
                                                                  **kwargs)


def get_db_adaptor():
    """
    A factory to get a DBAdapter object
    """
    return DBAdapter()


async def clear_repositories():
    """
    Drops repositories collection. Raises errors.InternalServerError
    is any errors.
    """
    try:
        db_adaptor = get_db_adaptor()
        await db_adaptor.drop(COLLECTION_NAME)
    except Exception as e:
        await log_exception(e)
        raise errors.InternalServerError()


async def delete_by_uuid(repo_uuid):
    """
    Deletes a repository from collection given by repo_uuid.
    Raises errors.InternalServerError is any errors.
    """
    try:
        db_adaptor = get_db_adaptor()
        # Check that repository is actually present
        search_filter = {'uuid': repo_uuid}
        document = await db_adaptor.find_one(COLLECTION_NAME,
                                             filter=search_filter)
        if not document:
            raise errors.ResourceNotFound()

        # Delete the repository
        result = await db_adaptor.delete_one(COLLECTION_NAME, search_filter)
        if not result.acknowledged:
            raise errors.InternalServerError()
    except Exception as e:
        await log_exception(e)
        raise errors.InternalServerError()


async def read_by_uuid(repo_uuid):
    """
    Reads single repository from collection given by repo_uuid.
    Raises errors.ResourceNotFound if repository is not found by repo_uuid.
    Raises errors.InternalServerError is any other errors.
    """
    try:
        db_adaptor = get_db_adaptor()
        search_filter = {'uuid': repo_uuid}
        document = await db_adaptor.find_one(COLLECTION_NAME,
                                             filter=search_filter,
                                             projection={'_id': False})
        if not document:
            raise errors.ResourceNotFound()
        return document
    except errors.ResourceNotFound as e:
        await log_exception(e)
        raise
    except Exception as e:
        await log_exception(e)
        raise errors.InternalServerError()


async def repositories_count():
    """
    Returns current size of repositories collection.
    Raises errors.InternalServerError is any other errors.
    """
    db_adaptor = get_db_adaptor()
    try:
        total_items = await db_adaptor.count(COLLECTION_NAME)
        return total_items
    except Exception as e:
        await log_exception(e)
        raise errors.InternalServerError()


async def update_repository(repo_uuid, update_object):
    """
    Updates single repository by given repo_uuid and update_object.
    Raises errors.ResourceNotFound if repository is not found by repo_uuid.
    Raises errors.InternalServerError is any other errors.
    """

    try:
        db_adaptor = get_db_adaptor()
        search_filter = {'uuid': repo_uuid}
        fields = {'_id': False}
        document = await db_adaptor.find_one_and_update(
            COLLECTION_NAME,
            search_filter,
            {'$set': update_object},
            projection=fields,
            return_document=ReturnDocument.AFTER
        )
        if not document:
            raise errors.InternalServerError()
        return document
    except errors.ResourceNotFound as e:
        await log_exception(e)
        raise
    except Exception as e:
        await log_exception(e)
        raise errors.InternalServerError()


async def pagenated_repositories(page=None, per_page=None, sort=None):
    """
    Reads paginated collection given by page, per_page, sort.
    Raises errors.InternalServerError is any other errors.
    """
    try:
        db_adaptor = get_db_adaptor()
        skip = per_page * (page - 1)
        fields = {'_id': False}
        search_cursor = db_adaptor.find_cursor(COLLECTION_NAME,
                                               projection=fields,
                                               skip=skip,
                                               limit=per_page,
                                               sort=sort)
        search_results = await search_cursor.to_list(None)
        return search_results
    except Exception as e:
        await log_exception(e)
        raise errors.InternalServerError()
