"""
This is a collection of different validators (request query, body, methods).
"""
import re

from jsonschema import validate, ValidationError

from gitstars.utils import log_exception
from gitstars import errors
from gitstars.const import (
    SUPPORTED_CONTENT_TYPE,
    DEFAULT_SORT_KEY,
    ALLOWED_SORTINGS,
    ORDER_ASCENDING,
    ORDER_DESCENDING,
    DEFAULT_PER_PAGE,
    MAX_PER_PAGE,
    UUID_MATCH_PATTERN
)


def validate_accept_header(accept_header, supported_conten_type):
    if supported_conten_type not in accept_header.lower():
        raise errors.AcceptTypeError()


def validate_request_method(method, supported_methods):
    if method not in supported_methods:
        raise errors.WrongMethod()


def validate_request_headers(headers):
    accept_header = headers.get('Accept')
    if accept_header:
        validate_accept_header(accept_header, SUPPORTED_CONTENT_TYPE)

    content_type_header = headers.get('Content-Type')
    if content_type_header:
        validate_content_type_header(content_type_header,
                                     SUPPORTED_CONTENT_TYPE)


def validate_repo_uuid(repo_uuid):
    valid_repo_uuid = bool(re.match(UUID_MATCH_PATTERN, repo_uuid))
    if not valid_repo_uuid:
        raise errors.BadRequestQuery()


def validate_content_type_header(content_type_header,
                                 supported_conten_type):
    if supported_conten_type not in content_type_header.lower():
        raise errors.ContentTypeError()


async def validate_json_object(obj, spec):
    try:
        validate(obj, spec)
    except ValidationError as e:
        # Workaround when description: null (not a string)
        if "None is not of type 'string'" in e.message:
            obj['description'] = ''
            msg = 'description: null (not a string) GitHubs issue...'
            raise errors.GitHubNullDescription(msg)
        await log_exception(e)
        raise errors.BadRequestBody()


async def validate_and_parse_sorting(query_parameters):
    try:
        sort_parameters = query_parameters.get('sort', DEFAULT_SORT_KEY)

        # Split sorting parameters into groups
        sort_array = sort_parameters.split(',')
        sort_pairs = [parameter.split('|') for parameter in sort_array]

        # Check that all sorting keys are allowed
        key_allowed = (pair[1] in ALLOWED_SORTINGS for pair in sort_pairs)
        if not all(key_allowed):
            msg = 'Only: {} sorting orders allowed.'.format(ALLOWED_SORTINGS)
            raise ValueError(msg)

        # Change strings 'asc' and 'desc' to DB sorting keys
        for pair in sort_pairs:
            if pair[1] == 'asc':
                pair[1] = ORDER_ASCENDING
            elif pair[1] == 'desc':
                pair[1] = ORDER_DESCENDING
        return [tuple(pair) for pair in sort_pairs]
    except Exception as e:
        await log_exception(e)
        raise errors.BadRequestQuery()


async def validate_and_parse_pagination(query_parameters):
    try:
        page = int(query_parameters.get('page', 1))
        per_page = int(query_parameters.get('per_page', DEFAULT_PER_PAGE))
        if page <= 0 or per_page <= 0:
            raise ValueError
        if per_page > MAX_PER_PAGE:
            per_page = MAX_PER_PAGE
        return page, per_page
    except Exception as e:
        await log_exception(e)
        raise errors.BadRequestQuery()
