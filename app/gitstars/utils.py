"""
This is a collection of different utils and helpers.
"""
import asyncio
import hashlib
import os
import re
import ujson
from logging import getLogger
from asyncio import sleep
from urllib.parse import urlencode, urlunparse
from email._parseaddr import mktime_tz, parsedate_tz

import aiohttp
from aiohttp import web

from gitstars import errors

from gitstars.const import (
    GITHUB_REQUIRED_FIELDS,
    GITHUB_LINK_REL_RE,
    GITHUB_LINK_URL_RE,
    DEFAULT_WEB_SCHEME,
    DEFAULT_WEB_HOST,
    DEFAULT_WEB_PORT,
    DEFAULT_WEB_BASE,
    DEFAULT_DB_HOST,
    DEFAULT_DB_PORT,
    GITHUB_SCHEME,
    GITHUB_NETLOC,
    GITHUB_SEARCH_PATH,
    GITHUB_SEARCH_QUERY
)


def construct_url(scheme=None,
                  netloc=None,
                  path=None,
                  params=None,
                  query=None,
                  fragment=None):
    if isinstance(query, (dict, tuple)):
        query = urlencode(query, safe='+')
    return urlunparse((scheme, netloc, path, params, query, fragment))


def github_url(scheme=GITHUB_SCHEME, netloc=GITHUB_NETLOC,
               path=GITHUB_SEARCH_PATH, query=GITHUB_SEARCH_QUERY):
    search_url = construct_url(scheme=scheme, netloc=netloc,
                               path=path, query=query)
    return search_url


def _construct_link_url(query):
    server_addr, _ = network_config()
    web_scheme, web_host, web_port, web_base = server_addr
    scheme = web_scheme
    netloc = web_host + ':' + str(web_port)
    path = web_base + '/repositories'
    return construct_url(scheme=scheme, netloc=netloc, path=path,
                         query=query)


def _construct_link_position(url, rel, **kwargs):
    link_position = {'href': url, 'rel': rel}
    for key, value in kwargs.items():
        if key not in {'href', 'rel'}:
            link_position[key] = value
    return link_position


def _construct_link_next(page, per_page):
    next_query = {'page': page + 1, 'per_page': per_page}
    next_url = _construct_link_url(next_query)
    return _construct_link_position(next_url, 'next')


def _construct_link_last(total_pages, per_page):
    last_query = {'page': total_pages, 'per_page': per_page}
    last_url = _construct_link_url(last_query)
    return _construct_link_position(last_url, 'last')


def _construct_link_first(per_page):
    first_query = {'page': 1, 'per_page': per_page}
    first_url = _construct_link_url(first_query)
    return _construct_link_position(first_url, 'first')


def _construct_link_prev(page, per_page):
    prev_query = {'page': page - 1, 'per_page': per_page}
    prev_url = _construct_link_url(prev_query)
    return _construct_link_position(prev_url, 'prev')


def construct_collections_links(page, per_page, total_pages):
    next_pos = _construct_link_next(page, per_page)
    last_pos = _construct_link_last(total_pages, per_page)
    first_pos = _construct_link_first(per_page)
    prev_pos = _construct_link_prev(page, per_page)

    if page == 1:
        return [next_pos, last_pos]

    if 1 < page < total_pages:
        return [first_pos, prev_pos, next_pos, last_pos]

    if page == total_pages:
        return [first_pos, prev_pos]

    if page > total_pages:
        return [first_pos, last_pos]


def construct_items_links(item):
    server_adrr, _ = network_config()
    web_scheme, web_host, web_port, web_base = server_adrr
    repo_uri = '/' + item['uuid']
    repo_url = construct_url(scheme=web_scheme,
                             netloc=web_host + ':' + str(web_port),
                             path=web_base + '/repositories' + repo_uri)
    methods = 'GET, PUT, DELETE'
    return _construct_link_position(repo_url, 'self', methods=methods)


def construct_repositories_rebuild():
    server_adrr, _ = network_config()
    web_scheme, web_host, web_port, web_base = server_adrr

    repos_url = construct_url(scheme=web_scheme,
                              netloc=web_host + ':' + str(web_port),
                              path=web_base + '/repositories')
    self_url = construct_url(scheme=web_scheme,
                             netloc=web_host + ':' + str(web_port),
                             path=web_base + '/rebuild-repositories')
    rebuild_obj = {
        'ETA': 'unknown',
        'reason': 'Rebuilding repositories database',
        'links': [_construct_link_position(self_url, 'self', method='POST'),
                  _construct_link_position(repos_url, 'result', method='GET')]
    }
    return rebuild_obj


def _parse_github_links_header(response_headers):
    links = response_headers['Link'].split(',')
    links_mapping = dict()

    for link in links:
        rel = re.findall(GITHUB_LINK_REL_RE, link)[0]
        url = re.findall(GITHUB_LINK_URL_RE, link)[0]
        links_mapping[rel] = url

    return links_mapping


def _clear_github_repo_response(full_repos):
    clear_repos = [{key: value
                    for key, value in repo.items()
                    if key in GITHUB_REQUIRED_FIELDS}
                   for repo in full_repos]
    return clear_repos


async def _github_request_limit_controller(response_headers):
    remaining = int(response_headers['X-RateLimit-Remaining'])
    if remaining == 0:
        server_time = response_headers['Date']
        # Parse RFC 2822 format Date
        server_timestamp = mktime_tz(parsedate_tz(server_time))
        reset = int(response_headers['X-RateLimit-Reset'])
        delay = reset - server_timestamp
        await sleep(delay)


def get_digest(message):
    """
    Returns message auth code as a base64 encoded string

    :param message:
    :return:
    """
    digest_alg = hashlib.new('sha1')
    message_as_bytes = bytes(message, 'utf-8')
    digest_alg.update(message_as_bytes)
    return digest_alg.hexdigest()


async def github_fetcher(client_session, url, aggregator):
    # About the Search API
    # The Search API is optimized to help you find_cursor the specific item
    # you're looking for (e.g., a specific user, a specific file in a
    # repositories, etc.). Think of it the way you think of performing a
    # search on Google. It's designed to help you find_cursor the one result
    # you're looking for (or maybe the few results you're looking for).
    # Just like searching on Google, you sometimes want to see a few pages
    # of search results so that you can find_cursor the item that best meets your
    # needs. To satisfy that need,
    # the GitHub Search API provides up to 1,000 results for each search.
    #                                ===================================
    #
    # {
    #   "message": "Only the first 1000 search results are available",
    #   "documentation_url": "https://developer.github.com/v3/search/"
    # }
    #

    # ¯\_(ツ)_/¯
    try:
        async with client_session.get(url) as response:
            if response.status != 200:
                raise aiohttp.ClientResponseError

            result = await response.json(loads=ujson.loads)
            result_headers = response.headers
            full_repos = result['items']
            cleared_repos = _clear_github_repo_response(full_repos)
            links_mapping = _parse_github_links_header(result_headers)
            await _github_request_limit_controller(result_headers)

            if 'next' not in links_mapping:
                # Base case
                aggregator.extend(cleared_repos)
                return aggregator
            else:
                aggregator.extend(cleared_repos)
                return await github_fetcher(client_session,
                                            links_mapping['next'],
                                            aggregator)
    except aiohttp.ClientResponseError as e:
        await log_exception(e)
        raise errors.ExternalResource()

    except Exception as e:
        await log_exception(e)
        raise errors.InternalServerError()


def json_response(obj, status=200, headers=None):
    json_text = ujson.dumps(obj,
                            indent=2,
                            sort_keys=True,
                            ensure_ascii=False,
                            escape_forward_slashes=False)

    return web.Response(text=json_text, content_type='application/json',
                        status=status, headers=headers)


def network_config():
    env_web_scheme = os.getenv('GS_WEB_SCHEME')
    web_scheme = env_web_scheme if env_web_scheme else DEFAULT_WEB_SCHEME

    env_web_host = os.getenv('GS_WEB_HOST')
    web_host = env_web_host if env_web_host else DEFAULT_WEB_HOST

    env_web_port = os.getenv('GS_WEB_PORT')
    web_port = int(env_web_port) if env_web_port else DEFAULT_WEB_PORT

    env_web_base = os.getenv('GS_WEB_BASE')
    web_base = env_web_base if env_web_base else DEFAULT_WEB_BASE

    env_db_host = os.getenv('GS_DB_HOST')
    db_host = env_db_host if env_db_host else DEFAULT_DB_HOST

    env_db_port = os.getenv('GS_DB_PORT')
    db_port = int(env_db_port) if env_db_port else DEFAULT_DB_PORT

    return (web_scheme, web_host, web_port, web_base), (db_host, db_port)


async def log_exception(exception):
    # TODO: Logging may block event loop - run in thread executor
    logger = getLogger()
    logger.exception(exception)


def json_error_response(e):
    if not isinstance(e, errors.ServerError):
        raise RuntimeError('Only ServerError type of exceptions are accepted!')
    error_object = construct_error_object(e.name_json,
                                          e.debug_id_json,
                                          e.message_json,
                                          e.information_link_json,
                                          e.links_json)
    return json_response(error_object, status=e.status_code)


async def send_many(chunk, client_session):
    server_adrr, _ = network_config()
    web_scheme, web_host, web_port, web_base = server_adrr

    url = construct_url(scheme=web_scheme,
                        netloc=web_host + ':' + str(web_port),
                        path=web_base + '/repositories')
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    for repo in chunk:
        async with client_session.post(url, json=repo,
                                       headers=headers) as response:
            if response.status != 200:
                # TODO: Decide strategy
                pass


async def post_many(repos, client_session, torrents=20):
    # print('qwe')
    chunks = [repos[i:i + torrents] for i in range(0, len(repos), torrents)]
    send_job = [send_many(chunk, client_session) for chunk in chunks]
    send_job_coro = asyncio.wait(send_job)
    await send_job_coro


def construct_error_object(name, debug_id, message,
                           information_link=None, links=None):
    error_json = {
        'name': name,
        'debug_id': debug_id,
        'message': message
    }

    if information_link:
        error_json['information_link'] = information_link
    if links:
        error_json['links'] = links
    return error_json
