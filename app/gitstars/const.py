"""
Basic constants goes here.
"""
GITHUB_SCHEME = 'https'
GITHUB_NETLOC = 'api.github.com'
GITHUB_SEARCH_PATH = '/search/repositories'
GITHUB_SEARCH_QUERY = {
    'q': 'language:python+stars:>=500',
    'sort': 'stars',
    'order': 'desc',
    'per_page': 100
}
GITHUB_LINK_REL_RE = 'rel=\"(.*)\"'
GITHUB_LINK_URL_RE = '<(.*)>'
GITHUB_REQUIRED_FIELDS = {
    'full_name',  # repo_owner str, repo_name str
    'html_url',  # repo_URL
    'description',  # repo_description
    'stargazers_count',  # repo_likes int
    'language'  # repo_language str
}

DEFAULT_PER_PAGE = 10
DEFAULT_SORT_KEY = 'stargazers_count|desc'
MAX_PER_PAGE = 100

DB_NAME = 'gitstar'
COLLECTION_NAME = 'repositories'
UUID_MATCH_PATTERN = 'REPO-[0-9A-F]{32}\Z'
ORDER_ASCENDING = 1
ORDER_DESCENDING = -1
ORDER_DEFAULT = ORDER_DESCENDING
ALLOWED_SORTINGS = {'asc', 'desc'}
SUPPORTED_CONTENT_TYPE = 'application/json'
SUPPORTED_METHODS = {
    '/repositories': {'GET', 'POST'},
    '/rebuild-repositories': {'POST'},
    '/repository': {'GET', 'PUT', 'DELETE'}
}
DEFAULT_WEB_SCHEME = 'http'
DEFAULT_WEB_HOST = 'localhost'
DEFAULT_WEB_PORT = 8080
DEFAULT_WEB_BASE = '/v1'
DEFAULT_DB_PORT = 27017
DEFAULT_DB_HOST = 'localhost'
