# Repository

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **str** |  | [optional] 
**digest** | **str** | SHA-1 message authentication code of required fields mapping. | [optional] 
**full_name** | **str** | Repository full name | 
**html_url** | **str** | Repository URL | 
**description** | **str** | A description of a repo | 
**stargazers_count** | **int** |  | 
**language** | **str** | That would be &#39;Python&#39; | 
**links** | [**Links**](Links.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


