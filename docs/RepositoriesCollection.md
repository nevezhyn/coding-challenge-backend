# RepositoriesCollection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_items** | **int** | Total items present in collection | 
**items** | [**list[Repository]**](Repository.md) |  | 
**total_pages** | **int** | Total pages available | 
**links** | [**Links**](Links.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


