# LinkDescription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **str** | a URI template, as defined by RFC 6570, with the addition of the $, ( and ) characters for pre-processing | 
**rel** | **str** | relation to the target resource of the link | 
**title** | **str** | a title for the link | [optional] 
**target_schema** | [****](.md) | JSON Schema describing the link target | [optional] 
**media_type** | **str** | media type (as defined by RFC 2046) describing the link target | [optional] [default to 'application/json']
**method** | **str** | method for requesting the target of the link (e.g. for HTTP this might be \&quot;GET\&quot; or \&quot;DELETE\&quot;) | [optional] [default to '(Optional) GET']
**enc_type** | **str** | The media type in which to submit data along with the request | [optional] [default to 'application/json']
**schema** | [****](.md) | Schema describing the data to submit along with the request | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


