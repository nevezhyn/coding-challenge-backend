# Rebuilding

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eta** | **str** |  | [optional] [default to 'unknown']
**reason** | **str** |  | [optional] [default to 'Rebuilding repositories database']
**links** | [**Links**](Links.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


