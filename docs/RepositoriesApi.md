# swagger_client.RepositoriesApi

All URIs are relative to *http://gitstars.nevezhyn.com:8080/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_repository**](RepositoriesApi.md#create_repository) | **POST** /repositories | Create a single repository record
[**delete_repository**](RepositoriesApi.md#delete_repository) | **DELETE** /repositories/{repository_uuid} | Delete a single repository record
[**get_repository**](RepositoriesApi.md#get_repository) | **GET** /repositories/{repository_uuid} | A single repository record
[**rebuild_repositories**](RepositoriesApi.md#rebuild_repositories) | **POST** /rebuild-repositories | WARNING: This operation is asynchronous! Rebuild all collection of repositories from scratch.
[**repositories**](RepositoriesApi.md#repositories) | **GET** /repositories | A collection of known repositories
[**update_repository**](RepositoriesApi.md#update_repository) | **PUT** /repositories/{repository_uuid} | Updates a single repository record


# **create_repository**
> Repository create_repository(body)

Create a single repository record

Creates a single repository in collection

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RepositoriesApi()
body = swagger_client.Repository() # Repository | Repository object that need to add to the store

try: 
    # Create a single repository record
    api_response = api_instance.create_repository(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->create_repository: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Repository**](Repository.md)| Repository object that need to add to the store | 

### Return type

[**Repository**](Repository.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_repository**
> delete_repository(repository_uuid)

Delete a single repository record

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RepositoriesApi()
repository_uuid = 'repository_uuid_example' # str | UUID of the repository to get

try: 
    # Delete a single repository record
    api_instance.delete_repository(repository_uuid)
except ApiException as e:
    print("Exception when calling RepositoriesApi->delete_repository: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **repository_uuid** | **str**| UUID of the repository to get | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_repository**
> Repository get_repository(repository_uuid)

A single repository record



### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RepositoriesApi()
repository_uuid = 'repository_uuid_example' # str | UUID of the repository to get

try: 
    # A single repository record
    api_response = api_instance.get_repository(repository_uuid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->get_repository: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **repository_uuid** | **str**| UUID of the repository to get | 

### Return type

[**Repository**](Repository.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rebuild_repositories**
> Rebuilding rebuild_repositories()

WARNING: This operation is asynchronous! Rebuild all collection of repositories from scratch.

Rebuild all collection of repositories from scratch

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RepositoriesApi()

try: 
    # WARNING: This operation is asynchronous! Rebuild all collection of repositories from scratch.
    api_response = api_instance.rebuild_repositories()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->rebuild_repositories: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Rebuilding**](Rebuilding.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **repositories**
> RepositoriesCollection repositories(page=page, per_page=per_page, sort=sort)

A collection of known repositories

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RepositoriesApi()
page = '1' # str | Page of the paginated results (optional) (default to 1)
per_page = '100' # str | Number of results per page (optional) (default to 100)
sort = 'sort_example' # str | A field to specify field to sort and sort order (`asc` or `desc`). Default sort order is descending. The pattern is following sort={field_name}|{asc|desc}. Multiple sorting pairs may be used (see example). (optional)

try: 
    # A collection of known repositories
    api_response = api_instance.repositories(page=page, per_page=per_page, sort=sort)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->repositories: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **str**| Page of the paginated results | [optional] [default to 1]
 **per_page** | **str**| Number of results per page | [optional] [default to 100]
 **sort** | **str**| A field to specify field to sort and sort order (&#x60;asc&#x60; or &#x60;desc&#x60;). Default sort order is descending. The pattern is following sort&#x3D;{field_name}|{asc|desc}. Multiple sorting pairs may be used (see example). | [optional] 

### Return type

[**RepositoriesCollection**](RepositoriesCollection.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_repository**
> Repository update_repository(repository_uuid, body)

Updates a single repository record

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.RepositoriesApi()
repository_uuid = 'repository_uuid_example' # str | UUID of the repository to get
body = swagger_client.Repository() # Repository | Repository object that need to be changed in the store

try: 
    # Updates a single repository record
    api_response = api_instance.update_repository(repository_uuid, body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RepositoriesApi->update_repository: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **repository_uuid** | **str**| UUID of the repository to get | 
 **body** | [**Repository**](Repository.md)| Repository object that need to be changed in the store | 

### Return type

[**Repository**](Repository.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

